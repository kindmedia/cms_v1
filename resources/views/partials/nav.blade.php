<div class="navbar-wrapper">
    <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{!! URL::to('') !!}">Project name</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="{{ (Request::is('/') ? 'active' : '') }}"><a href="{!! URL::to('') !!}">Home</a></li>
                        <li class="{{ (Request::is('about') ? 'active' : '') }}"><a href="{!! URL::to('about') !!}">About</a></li>
                        <li class="{{ (Request::is('contact') ? 'active' : '') }}"><a href="{!! URL::to('contact') !!}">Contact</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Nav header</li>
                                <li><a href="#">Separated link</a></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                        @if (Auth::guest())
                            <li class="{{ (Request::is('auth/login') ? 'active' : '') }}"><a href="{!! URL::to('auth/login') !!}"><i
                                            class="fa fa-sign-in"></i> Login</a></li>
                            <li class="{{ (Request::is('auth/register') ? 'active' : '') }}"><a
                                        href="{!! URL::to('auth/register') !!}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->name }} <i
                                            class="fa fa-caret-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    @if(Auth::check())
                                        @if(Auth::user()->admin==1)
                                            <li>
                                                <a href="{!! URL::to('admin/dashboard') !!}"><i class="fa fa-tachometer"></i> Dashboard</a>
                                            </li>
                                        @endif
                                        <li role="presentation" class="divider"></li>
                                    @endif
                                    <li>
                                        <a href="{!! URL::to('auth/logout') !!}"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

    </div>
</div>