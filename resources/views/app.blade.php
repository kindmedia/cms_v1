<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@section('title') Laravel Test Site @show</title>
    @section('meta_keywords')
        <meta name="keywords" content="test, testen, tester"/>
    @show @section('meta_author')
        <meta name="author" content="Jon Doe"/>
    @show @section('meta_description')
        <meta name="description"
              content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei."/>
        @show
    <!-- Global css Start -->
        <link href="{{ elixir('assets/css/site.css') }}" rel="stylesheet">
    <!-- //Global css End -->
    <!-- Site css Start -->
    @yield('styles')
    <!-- //Site css End -->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link rel="shortcut icon" href="{{{ asset('assets/img/favicon.ico') }}}">
</head>
<body>
@include('partials.nav')



@yield('content')

@include('partials.footer')
<!-- Global js Start -->
    <script src="{{ elixir('assets/js/site.js') }}"></script>
<!-- //Global js End -->
<!-- Site js Start -->
    @yield('scripts')
<!-- //Site js End -->
</body>
</html>