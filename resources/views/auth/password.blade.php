@extends('auth')
@section('title') Login :: @parent @stop

@section('styles')

@endsection

@section('content')
    @include('errors.list')
    <div class="container">

        <form class="form-signin" role="form" method="POST" action="{!! URL::to('/password/email') !!}">
            <h2 class="form-signin-heading">Reset Password</h2>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

        </form>

    </div>
@endsection

@section('scripts')

@endsection