@extends('auth')
@section('title') Login :: @parent @stop

@section('styles')

@endsection

@section('content')
    @include('errors.list')
    <div class="container">

        <form class="form-signin" role="form" method="POST" action="{!! URL::to('/auth/login') !!}">
            <h2 class="form-signin-heading">Please sign in</h2>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" value="remember"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <a href="{!! URL::to('/password/email') !!}">Forgot Your Password?</a>
        </form>

    </div>
@endsection

@section('scripts')

@endsection