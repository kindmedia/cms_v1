@extends('auth')
@section('title') Register :: @parent @stop

@section('styles')

@endsection

@section('content')
    @include('errors.list')
    <div class="container">

        <form class="form-signin" role="form" method="POST" action="{!! URL::to('/auth/register') !!}">
            <h2 class="form-signin-heading">Register</h2>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <label for="inputName" class="sr-only">Name</label>
            <input type="text" id="inputName" class="form-control" name="name" placeholder="Name">
            <label for="inputUserName" class="sr-only">UserName</label>
            <input type="text" id="inputUserName" class="form-control" name="username" placeholder="Username">
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <label for="confirmPassword" class="sr-only">Confirm Password</label>
            <input type="password" name="password_confirmation" id="confirmPassword" class="form-control" placeholder="Confirm Password" required>


            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>

    </div>
@endsection

@section('scripts')

@endsection