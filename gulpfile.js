var gulp = require("gulp");
var bower = require("gulp-bower");
var elixir = require("laravel-elixir");

gulp.task('bower', function() {
  return bower();
});

var paths = {
  'jquery': 'assets/vendor/jquery/dist',
  'bootstrap': 'assets/vendor/bootstrap/dist',
  'fontawesome': 'assets/vendor/font-awesome',
  'ionicons': 'assets/vendor/ionicons',
  'adminlte': 'assets/vendor/admin-lte'
};

elixir.config.sourcemaps = false;

elixir(function (mix) {
  mix.copy('resources/' + paths.bootstrap + '/fonts/**', 'public/assets/fonts');
  mix.copy('resources/' + paths.fontawesome + '/fonts/**', 'public/assets/fonts');
  mix.copy('resources/' + paths.ionicons + '/fonts/**', 'public/assets/fonts');

  mix.copy('resources/' + paths.bootstrap + '/fonts/**', 'public/build/assets/fonts');
  mix.copy('resources/' + paths.fontawesome + '/fonts/**', 'public/build/assets/fonts');
  mix.copy('resources/' + paths.ionicons + '/fonts/**', 'public/build/assets/fonts');
  mix.copy('resources/' + paths.adminlte + '/dist/img/**', 'public/img');
  // Merge Site CSSs.
  mix.styles([
      paths.bootstrap + '/css/bootstrap.min.css'
  ], 'public/assets/css/site.css', 'resources/');

  // Merge Admin CSSs.
  mix.styles([
      paths.bootstrap + '/css/bootstrap.min.css',
      paths.fontawesome + '/css/font-awesome.min.css',
      paths.ionicons + '/css/ionicons.min.css',
      paths.adminlte + '/dist/css/AdminLTE.min.css',
      paths.adminlte + '/dist/css/skins/skin-blue.min.css',
      paths.adminlte + '/plugins/iCheck/square/blue.css'
  ], 'public/assets/css/admin.css', 'resources/');

  // Merge Auth CSSs.
  mix.styles([
      paths.bootstrap + '/css/bootstrap.min.css',
      'assets/sites/css/login.css'
  ], 'public/assets/css/auth.css', 'resources/');

  // Merge Home CSSs.
  mix.styles([
      'assets/sites/css/carousel.css'
  ], 'public/assets/css/home.css', 'resources/');


  //Merge Site Scripts
  mix.scripts([
      paths.jquery + '/jquery.min.js',
      paths.bootstrap + '/js/bootstrap.min.js'
  ], 'public/assets/js/site.js', 'resources/' );

  //Merge Admin Scripts
  mix.scripts([
      paths.jquery + '/jquery.min.js',
      paths.bootstrap + '/js/bootstrap.min.js',
      paths.adminlte + '/dist/js/app.min.js'
  ], 'public/assets/js/admin.js', 'resources/');

  //Merge Site Scripts
  mix.scripts([
      paths.jquery + '/jquery.min.js',
      paths.bootstrap + '/js/bootstrap.min.js'
  ], 'public/assets/js/auth.js', 'resources/' );

  // Cache-bust all.css and all.js files.
  mix.version([
    'assets/css/site.css',
    'assets/css/admin.css',
    'assets/css/auth.css',
    'assets/css/home.css',
    'assets/js/site.js',
    'assets/js/admin.js',
    'assets/js/auth.js'
  ]);
});


